import time
from functools import wraps


def cache(func):
    @wraps(func)
    def wrapper(*args):
        if args in fib_cache:
            fib_cache[args] = fib_cache.get(args)[0], time.time()
            wrapper.cache_calls += 1
            wrapper.calls += 1
            return fib_cache.get(args)[0]
        else:
            result = func(*args)
            wrapper.calls += 1

        if len(fib_cache) == maxsize:
            min_val, value_to_remove = time.time(), 0
            for key, value in fib_cache.items():
                if value[1] < min_val:
                    min_val = value[1]
                    value_to_remove = key
            fib_cache.pop(value_to_remove)

        fib_cache[args] = result, time.time()
        time.sleep(0.02)
        return result

    def counter():
        counter.info['func calls counter'] = wrapper.calls
        counter.info['free'] = maxsize - len(fib_cache)
        counter.info['counter cache calls'] = wrapper.cache_calls
        counter.info['cache life time'] = time.time() - cache_time
        return print('CacheInfo:', counter.info)

    def cache_clear():
        fib_cache.clear()
        return print('Cache cleared', fib_cache)

    maxsize = 128
    fib_cache = {}
    cache_time = time.time()
    counter.info = {'func calls counter': 0, 'free': 0, 'counter cache calls': 0, 'cache life time': 0}
    wrapper.calls, wrapper.cache_calls = 0, 0
    wrapper.counter = counter
    wrapper.cache_clear = cache_clear
    return wrapper


@cache
def fib(n):
    """This function calculates the Fibonacci number"""
    fibonacci = [0, 1]
    for i in range(2, n + 1):
        fibonacci.append(fibonacci[i - 1] + fibonacci[i - 2])
    return fibonacci[n]


print(fib(10))
print(fib(5))
print(fib(10))
print(fib(7))
print(fib(2))

fib.counter()
fib.cache_clear()

print(fib.__name__)
print(fib.__doc__)
