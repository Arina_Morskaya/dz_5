import time


def timer(func):
    def timer_fib(*args):
        t = time.time()
        res = func(*args)
        print(f'Fibonacci calculation time: {time.time() - t}')
        return res
    return timer_fib


@timer
def fib(n):
    fibonacci = [0, 1]
    for i in range(2, n + 1):
        fibonacci.append(fibonacci[i - 1] + fibonacci[i - 2])
    return fibonacci[n]


fib(10000)
