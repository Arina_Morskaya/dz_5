import time


def fib_numbers(func):
    cache = {}

    def wrapper(*args):
        if args not in cache:
            cache[args] = func(*args)
            print(f'New Fibonacci number: {cache[args]}')
        else:
            print(f'Fibonacci number from the archive: {cache[args]}')
        return cache[args]
    return wrapper


@fib_numbers
def fib(n):
    fibonacci = [0, 1]
    for i in range(2, n + 1):
        fibonacci.append(fibonacci[i - 1] + fibonacci[i - 2])
    return fibonacci[n]


fib(7)
fib(10)
fib(7)

