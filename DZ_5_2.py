from functools import singledispatch


@singledispatch
def test(x, y):
    raise Exception('Incorrect data type')


@test.register(int)
def number(x, y):
    print('Arguments data type -', type(x), type(y))
    return x + y


@test.register(str)
def text(x, y):
    print('Arguments data type -', type(x), type(y))
    return x + y


print(test(1, 2))
print(test('He', 'llo'))
# print(test([123], [456]))
