def counter(func):
    def wrapper(*args):
        wrapper.calls += 1
        func(*args)
    wrapper.calls = 0
    return wrapper


def fib_numbers(func):
    def wrapper(*args):
        if args not in cache:
            cache[args] = func(*args)
        return cache[args]
    cache = {}
    return wrapper


@counter
@fib_numbers
def fib(n):
    fibonacci = [0, 1]
    for i in range(2, n + 1):
        fibonacci.append(fibonacci[i - 1] + fibonacci[i - 2])
    return fibonacci[n]


fib(5)
fib(50)
fib(500)
print(f'Counter: {fib.calls}')

